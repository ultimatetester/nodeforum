var NodeForum = {};
var express = require('express');
var routes = require('./routes');
var path = require("path");

NodeForum.initialize = function (app, path) {
    app.use('/forum_static', express.static(NodeForum.static()));
    app.use(path, NodeForum.routes());
};

NodeForum.static = function () {
    return path.join(__dirname, 'public');
};

NodeForum.routes = function () {
    return routes;
};

module.exports = NodeForum;