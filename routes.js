var express = require('express');
var path = require("path");
var router = express.Router();
var viewsFolder = path.join(__dirname, 'views');

router.get('/', function (req, res, next) {
    res.render(path.join(viewsFolder, 'categories'));
});

router.get('/category/:categoryId/', function (req, res, next) {
    res.render(path.join(viewsFolder, 'category'));
});

router.get('/category/:categoryId/thread/:threadId/', function (req, res, next) {
    res.render(path.join(viewsFolder, 'thread'));
});

router.get('/user/:userId/', function (req, res, next) {
    res.render(path.join(viewsFolder, 'user'));
});

router.post('/search/', function (req, res, next) {
    res.render(path.join(viewsFolder, 'searchLoading'));
});

module.exports = router;